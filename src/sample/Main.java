package sample;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import sample.FXMLController.*;

import java.util.Scanner;

public class Main extends Actions
{

    public static WebDriver chrome;

    public static char quit = 0;
    public static String response;
    public static int choice = 0;
    public static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws InterruptedException
    {
        System.setProperty("webdriver.chrome.driver","/Users/miszafarczyk/IdeaProjects/Bot for browser/src/sample/chromedriver.exe");
        chrome = new ChromeDriver();

        Selenium.Setup();
        Thread.sleep(2000);

        Actions.Login();
        Thread.sleep(2000);

        while (quit != 'n') {
            System.out.println("Choose a task for me:"
                    + "\n1. Build 1"
                    + "\n2. Build 2"
                    + "\n3. Train troops"
                    + "\n4. Upgrade troops");
            choice = scan.nextInt();

            switch (choice) {
                case 1:
                    Actions.Build1();
                    break;
                    case 2:
                        Actions.changeVillage();
                        Actions.Build2();
                        break;

                    case 3:
                        Actions.changeVillage();
                        Actions.trainTroops();
                        break;

                    case 4:
                        Actions.changeVillage();
                        Actions.upgradeTroops();
                        break;

                    default:
                        System.out.println("Wrong answer");
                }

                System.out.println("Would you like to make another action? y/n");
                response = scan.next().toLowerCase();
                quit = response.charAt(0);
        }
    }
}
