package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class LoginApp extends Application
{

    public static String login2, password2, email2, server2;

    @Override
    public void start(Stage primaryStage)
    {
        primaryStage.setTitle("Bot for Browser");
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Text scenetitle = new Text("Login to your account");
        scenetitle.setFont(Font.font("Tahoma", 14.0));
        grid.add(scenetitle, 0, 0, 2, 1);

        Label userName = new Label("Name:");
        grid.add(userName,0,1);

        TextField userTextField = new TextField(login2);
        grid.add(userTextField, 1,1);

        Label password = new Label("Password:");
        grid.add(password, 0, 2);

        TextField passwordTextField = new TextField(password2);
        grid.add(passwordTextField, 1, 2);

        Label serverName = new Label("Server");
        grid.add(serverName,0,3);

        TextField serverTextField = new TextField(server2);
        grid.add(serverTextField, 1,3);


        Label emailName = new Label("Email");
        grid.add(emailName,0,4);

        TextField emailTextField = new TextField(email2);
        grid.add(emailTextField, 1,4);

        Button btn = new Button("Sign in");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(btn);
        grid.add(hbBtn, 1, 6);

        final Text actionTarget = new Text();
        grid.add(actionTarget, 1, 8);

        btn.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent event)
            {
                actionTarget.setFill(Color.FIREBRICK);
                actionTarget.setText("Singing in...");
            }
        });

        Scene scene = new Scene(grid, 700, 550);
        primaryStage.setScene(scene);
        primaryStage.show();


    }
}
