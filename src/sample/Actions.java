package sample;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Scanner;

import static sample.Main.chrome;

public class Actions extends Selenium
{

    public static boolean isLoggedIn = false;

    public static String task, troopA;
    public static int building, building2, j, troop, troopU, u, villager;

    public static void Login() throws InterruptedException
    {

        chrome.get("https://" + server + ".molon-lave.net/");
        Thread.sleep(1500);

        WebElement playBox = chrome.findElement(By.name("user"));
        playBox.click();
        playBox.sendKeys(login);
        Thread.sleep(1000);

        WebElement playBox2 = chrome.findElement(By.name("pw"));
        playBox2.click();
        playBox2.sendKeys(password);
        Thread.sleep(1000);

        WebElement log = chrome.findElement(By.className("button-content"));
        log.click();
        Thread.sleep(1000);
    }

    public static void Build1() throws InterruptedException
    {
        Thread.sleep(1000);
        chrome.get("https://tx3.molon-lave.net/dorf1.php");
        Thread.sleep(1000);

        Scanner build = new Scanner(System.in);
        System.out.println("How many times you want to run this loop?");
        building = build.nextInt();

        for (i = 0; i < building; i++)
        {
            chrome.get("https://tx3.molon-lave.net/build.php?id=1&fastUP=1");
            Thread.sleep(building*1000);
            chrome.get("https://tx3.molon-lave.net/build.php?id=2&fastUP=1");
            Thread.sleep(building*1000);
            chrome.get("https://tx3.molon-lave.net/build.php?id=3&fastUP=1");
            Thread.sleep(building*1000);
            chrome.get("https://tx3.molon-lave.net/build.php?id=4&fastUP=1");
            Thread.sleep(building*1000);
            chrome.get("https://tx3.molon-lave.net/build.php?id=5&fastUP=1");
            Thread.sleep(building*1000);
            chrome.get("https://tx3.molon-lave.net/build.php?id=6&fastUP=1");
            Thread.sleep(building*1000);
            chrome.get("https://tx3.molon-lave.net/build.php?id=7&fastUP=1");
            Thread.sleep(building*1000);
            chrome.get("https://tx3.molon-lave.net/build.php?id=8&fastUP=1");
            Thread.sleep(building*1000);
            chrome.get("https://tx3.molon-lave.net/build.php?id=9&fastUP=1");
            Thread.sleep(building*1000);
            chrome.get("https://tx3.molon-lave.net/build.php?id=10&fastUP=1");
            Thread.sleep(building*1000);
            chrome.get("https://tx3.molon-lave.net/build.php?id=11&fastUP=1");
            Thread.sleep(building*1000);
            chrome.get("https://tx3.molon-lave.net/build.php?id=12&fastUP=1");
            Thread.sleep(building*1000);
            chrome.get("https://tx3.molon-lave.net/build.php?id=13&fastUP=1");
            Thread.sleep(building*1000);
            chrome.get("https://tx3.molon-lave.net/build.php?id=14&fastUP=1");
            Thread.sleep(building*1000);
            chrome.get("https://tx3.molon-lave.net/build.php?id=15&fastUP=1");
            Thread.sleep(building*1000);
            chrome.get("https://tx3.molon-lave.net/build.php?id=16&fastUP=1");
            Thread.sleep(building*1000);
            chrome.get("https://tx3.molon-lave.net/build.php?id=17&fastUP=1");
            Thread.sleep(building*1000);
            chrome.get("https://tx3.molon-lave.net/build.php?id=18&fastUP=1");
            Thread.sleep(building*1000);
            System.out.println("Loop ran" + " " + i + " " + "times");
        }
    }

    public static void Build2() throws InterruptedException
    {
        Thread.sleep(1000);
        chrome.get("https://tx3.molon-lave.net/dorf2.php");
        Thread.sleep(1000);

        Scanner build2 = new Scanner(System.in);
        System.out.println("What you want to build?"
        + "\n1. Main Building"
                + "\n2. Rally Point"
        + "\n3. Barracks"
        + "\n4. Warehouse"
        + "\n5. Granary"
        + "\n6. Marketplace"
        + "\n7. Hero Mansion"
        + "\n8. Academy"
        + "\n9. Smithy"
        + "\n10. Stable"
                + "\n11. Wall"
        + "\n12. Building id: 19"
        + "\n13. Building id: 20"
        + "\n14. Building id: 21"
                + "\n15. Building id: 22"
                + "\n16. Building id: 23"
                + "\n17. Building id: 31"
                + "\n18. Building id: 30"
                + "\n19. Building id: 27"
                + "\n20. Building id: 34"
                + "\n21. Building id: 29"
        + "\n22. Building id: 36");

        building2 = build2.nextInt();

        switch (building2)
        {
            case 1:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=26&fastUP=1");
                }
                System.out.println("Main Building completed");
                break;

            case 2:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=39&fastUP=1");
                }
                System.out.println("Rally Point completed");
                break;

            case 3:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=32&fastUP=1");
                }
                System.out.println("Barracks completed");
                break;

            case 4:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=24&fastUP=1");
                }
                System.out.println("Warehouse completed");
                break;

            case 5:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=28&fastUP=1");
                }
                System.out.println("Granary completed");
                break;

            case 6:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=35&fastUP=1");
                }
                System.out.println("Marketplace completed");
                break;

            case 7:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=38&fastUP=1");
                }
                System.out.println("Hero Mansion completed");
                break;

            case 8:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=25&fastUP=1");
                }
                System.out.println("Academy completed");
                break;

            case 9:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=37&fastUP=1");
                }
                System.out.println("Smithy completed");
                break;

            case 10:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=33&fastUP=1");
                }
                System.out.println("Stable completed");
                break;

            case 11:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=40&fastUP=1");
                }
                System.out.println("Wall completed");
                break;

            case 12:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=19&fastUP=1");
                }
                System.out.println("Building id: 19 completed");
                break;

            case 13:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=20&fastUP=1");
                }
                System.out.println("Building id: 20 completed");
                break;

            case 14:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=21&fastUP=1");
                }
                System.out.println("Building id: 21 completed");
                break;

            case 15:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=22&fastUP=1");
                }
                System.out.println("Building id: 22 completed");
                break;

            case 16:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=23&fastUP=1");
                }
                System.out.println("Building id: 23 completed");
                break;

            case 17:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=31&fastUP=1");
                }
                System.out.println("Building id: 31 completed");
                break;

            case 18:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=30&fastUP=1");
                }
                System.out.println("Building id: 30 completed");
                break;

            case 19:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=27&fastUP=1");
                }
                System.out.println("Building id: 27 completed");
                break;

            case 20:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=34&fastUP=1");
                }
                System.out.println("Building id: 34 completed");
                break;

            case 21:
            Thread.sleep(1000);
            for (j = 0; j < 20; j++)
            {
                Thread.sleep(3000);
                chrome.get("https://tx3.molon-lave.net/build.php?id=29&fastUP=1");
            }
            System.out.println("Building id: 29 completed");
            break;

            case 22:
                Thread.sleep(1000);
                for (j = 0; j < 20; j++)
                {
                    Thread.sleep(3000);
                    chrome.get("https://tx3.molon-lave.net/build.php?id=36&fastUP=1");
                }
                System.out.println("Building id: 36 completed");
                break;

                default:
                    System.out.println("Wrong answer");
        }

    }

    public static void trainTroops() throws InterruptedException
    {
        Thread.sleep(1000);
        Scanner troops = new Scanner(System.in);
        System.out.println("What type of troops you want to train?"
                + "\n1. Clubs"
                + "\n2. Spearmen"
                + "\n3. Axemen"
                + "\n4. Scouts"
                + "\n5. Paladins"
                + "\n6. Teutonic Knights"
                + "\n7. Rams"
                + "\n8. Catapults");
        troop = troops.nextInt();

        System.out.println("How many of them you want to train?");
        troopA = troops.next();

        switch (troop)
        {
            case 1:
                Thread.sleep(1000);
                chrome.get("https://tx3.molon-lave.net/build.php?id=32&fastUP=0");

                WebElement trainBox = chrome.findElement(By.name("t1"));
                trainBox.click();
                trainBox.sendKeys(troopA);

                WebElement trainButton = chrome.findElement(By.name("s1"));
                trainButton.click();

                System.out.println("Started to train" + troopA + "clubs");
                break;

            case 2:
                Thread.sleep(1000);
                chrome.get("https://tx3.molon-lave.net/build.php?id=32&fastUP=0");

                WebElement trainBox2 = chrome.findElement(By.name("t2"));
                trainBox2.click();
                trainBox2.sendKeys(troopA);

                WebElement trainButton2 = chrome.findElement(By.name("s1"));
                trainButton2.click();

                System.out.println("Started to train" + troopA + "spearmen");
                break;

            case 3:
                Thread.sleep(1000);
                chrome.get("https://tx3.molon-lave.net/build.php?id=32&fastUP=0");

                WebElement trainBox3 = chrome.findElement(By.name("t3"));
                trainBox3.click();
                trainBox3.sendKeys(troopA);

                WebElement trainButton3 = chrome.findElement(By.name("s1"));
                trainButton3.click();

                System.out.println("Started to train" + troopA + "axemen");
                break;

            case 4:
                Thread.sleep(1000);
                chrome.get("https://tx3.molon-lave.net/build.php?id=32&fastUP=0");

                WebElement trainBox4 = chrome.findElement(By.name("t4"));
                trainBox4.click();
                trainBox4.sendKeys(troopA);

                WebElement trainButton4 = chrome.findElement(By.name("s1"));
                trainButton4.click();

                System.out.println("Started to train" + troopA + "scouts");
                break;

            case 5:
                Thread.sleep(1000);
                chrome.get("https://tx3.molon-lave.net/build.php?id=33&fastUP=0");

                WebElement trainBox5 = chrome.findElement(By.name("t5"));
                trainBox5.click();
                trainBox5.sendKeys(troopA);

                WebElement trainButton5 = chrome.findElement(By.name("s1"));
                trainButton5.click();

                System.out.println("Started to train" + troopA + "paladins");
                break;

            case 6:
                Thread.sleep(1000);
                chrome.get("https://tx3.molon-lave.net/build.php?id=33&fastUP=0");

                WebElement trainBox6 = chrome.findElement(By.name("t6"));
                trainBox6.click();
                trainBox6.sendKeys(troopA);

                WebElement trainButton6 = chrome.findElement(By.name("s1"));
                trainButton6.click();

                System.out.println("Started to train" + troopA + "teutonic knights");
                break;

            case 7:
                Thread.sleep(1000);
                chrome.get("https://tx3.molon-lave.net/build.php?id=29&fastUP=0");

                WebElement trainBox7 = chrome.findElement(By.name("t7"));
                trainBox7.click();
                trainBox7.sendKeys(troopA);

                WebElement trainButton7 = chrome.findElement(By.name("s1"));
                trainButton7.click();

                System.out.println("Started to train" + troopA + "rams");
                break;

            case 8:
                Thread.sleep(1000);
                chrome.get("https://tx3.molon-lave.net/build.php?id=29&fastUP=0");

                WebElement trainBox8 = chrome.findElement(By.name("t8"));
                trainBox8.click();
                trainBox8.sendKeys(troopA);

                WebElement trainButton8 = chrome.findElement(By.name("s1"));
                trainButton8.click();

                System.out.println("Started to train" + troopA + "catapults");
                break;

            default:
                System.out.println("Wrong answer");
        }
    }

    public static void upgradeTroops() throws InterruptedException
    {
        Thread.sleep(1000);
        chrome.get("https://tx3.molon-lave.net/build.php?id=37&fastUP=0");
        Thread.sleep(1000);

        WebElement buttonUpgrade = chrome.findElement(By.xpath("//*[@id=\"buttonaWDlCxi7kjuHq\"]/div/div[2]"));

        for (u = 0; u < 20; u++)
        {
            buttonUpgrade.click();
            Thread.sleep(3000);
        }


    }

    public static void changeVillage() throws InterruptedException
    {
        Thread.sleep(1000);

        Scanner village = new Scanner(System.in);
        System.out.println("Type name of your village, you want to switch");
        villager = village.nextInt();

        /*WebElement vill = chrome.findElement(By.xpath("//*[text() = '00. Z kopyta']"));
        vill.click();*/

        switch (villager)
        {
            case 0:
                Thread.sleep(500);
                chrome.get("https://tx3.molon-lave.net/dorf1.php?newdid=24861");
                break;

            case 1:
                Thread.sleep(500);
                chrome.get("https://tx3.molon-lave.net/dorf1.php?newdid=24060");
                break;

            case 2:
                Thread.sleep(500);
                chrome.get("https://tx3.molon-lave.net/dorf1.php?newdid=23452");
                break;

            case 3:
                Thread.sleep(500);
                chrome.get("https://tx3.molon-lave.net/dorf1.php?newdid=23247");
                break;

            case 4:
                Thread.sleep(500);
                chrome.get("https://tx3.molon-lave.net/dorf1.php?newdid=23060");
                break;

            case 5:
                Thread.sleep(500);
                chrome.get("https://tx3.molon-lave.net/dorf1.php?newdid=24458");
                break;

            case 99:
                Thread.sleep(500);
                chrome.get("https://tx3.molon-lave.net/dorf1.php?newdid=26869");
                break;

            /*case 6:
                Thread.sleep(500);
                chrome.get("https://tx3.molon-lave.net/dorf1.php?newdid=24060");
                break;*/


                default:
                    System.out.println("Wrong answer");
        }
    }

}
